# HTML AVANZADO [Tablas]

1. Diseño semántico de una tabla en HTML5.
   | Etiquetas | Descripción|
   |-------------|----------|
   | **< table >**| Definición de una tabla.|
   | **< th >** | Definición de cabecera de tabla.|
   | **< tr >** | Define una fila en la tabla. |
   | **< td >**| Define una celda en la tabla. |
   | **caption** | Define el nombre o título de la tabla.|
   | **colgroup** | Especifica un grupo de una o más columnas para aplicar formato.|
   | **col** | Especifica las propiedades de una columna de las columnas definidas en un elemento colgroup.|
   | **thead** | Define la cabecera de la tabla.|
   | **tbody** | Define el cuerpo de la tabla.|
   | **tfoot** | Define el pie de la tabla.|

2. Imagen representativa de la tabla.

   ![Descripción de la imagen](tabla.PNG)

3. Código:

```html
<body>
  <main>
    <table border="3">
      <!--Titulo de la tabla -->
      <caption>
        Tabla de animales
      </caption>
      <!--Encabezado de la tabla -->
      <thead>
        <tr>
          <th>Mascota</th>
          <th>Edad</th>
          <th>Color</th>
        </tr>
      </thead>
      <!--Cuerpo de la tabla -->
      <tbody>
        <tr>
          <td>Gato</td>
          <td>3 años</td>
          <td>Negro</td>
        </tr>
        <tr>
          <td>Perro</td>
          <td>1 mes</td>
          <td>Azul</td>
        </tr>
      </tbody>
      <!-- Pie de tabla -->
      <tfoot>
        <tr>
          <th>Mascota</th>
          <th>Edad</th>
          <th>Color</th>
        </tr>
      </tfoot>
    </table>
  </main>

  <footer></footer>
</body>
```
